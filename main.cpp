/****************************************************************
 * Artificial Neural Networks
 * Exercise 03
 *
 * Benjamin Lentz 108012209500
 * Emanuel Durmaz 108012234853
 *
 ****************************************************************

c)

Ein geeignetes Stoppkriterium wäre, den Fehler zwischen der letzten und der aktuellen Iteration zu vergleichen.
Wenn sich dieser Fehler nicht oder nur sehr geringfügig ändert, kann davon ausgegangen werden dass der Algorithmus konvergiert ist.
In der Praxis wird die Differenz der beiden Fehlerwerte bestimmt. Wenn diese Differenz unter einer vordefinierten Schranke epsilon liegt,
ist das Konvergenzkriterium erfüllt.
Des weiteren kann man davon ausgehen, wenn die maximale Anzahl an Iterationen erreicht wird, dass der Divergenzfall eingetreten ist.


d)

Die Lernrate wurde in 0,07 Schritten von 0.01 bis 0.50 erhöht. Für Lernraten unter 0.43 konvergierte der Algorithmus immer.
Bei einer Lernrate von eta = 0.43 und 0.5 hingegen divergierte er in der Mehrzahl der Fälle.
Je nachdem, wie die Lernrate eingestellt war, wurden unterschiedlich viele Iterationen für eine Konvergenz benötigt.
Weder zu niedrige noch zu hohe Werte scheinen ein optimales Konvergenzverhalten zu verursachen.
In unserem Fall lag die niedrigste Anzahl von benötigten Iterationen bei eta = 0.22.
Je weniger Iterationen ausgeführt werden, desto kürzer ist die benötigte Laufzeit.
Deshalb sollte vor allem der Fall umgangen werden, dass der Algorithmus divergiert.


e)

Wenn ein Wert von w gleich null gesetzt wird ergibt die Ableitung des Fehlers nach diesem w auch null.
Dies führt dazu dass sich der entsprechende w Wert über die Iterationen nicht ändert,
da die besagte Ableitung für die Berechnung des gradient descents benötigt wird.

Wenn w_init als Nullvektor definiert wird, tritt so das Konvergenzkriterium sofort ein,
da die Fehlerdifferenz bei den gleichbleibenden Werten von w gleich null ist.
An dieser Stelle befindet sich allerdings nicht wirklich ein Minimum, wie wir daraus wissen, dass
eine hinreichende Bedingung aus Aufgabenteil a) dazu nicht erfüllt ist. Nämlich dass alpha > - beta^2

Wenn die Startwerte für w allerdings definiert werden mit w_init = (1.5, 1.5, 0),
werden immerhin zwei der drei w Werte in jeder Iteration geändert,
sodass ein Minimum gefunden werden kann für welches w3=0 ist.
Bei Analyse der Hessematrix wird deutlich, dass es sich bei diesem Punkt tatsächlich um ein Minimum handelt.


 */


// @note: We don't need M_PI this time. (Reality will collapse.)

#include "matrix.h"

#include <cmath>
#include <iostream>
#include <limits>


// @note: Functions in this file are not put in namespace  ann  anymore.
//        Also, you don't need to bother with putting the namespace  ann  in
//        front of any of the types. You can simply use:
//        matrix,  real_t,  size_t
typedef ann::matrix matrix;
typedef matrix::real_t real_t;
typedef matrix::size_t size_t;

//
// @note: Square a value without using anoying pow or multiplying with itself everytime.
//        Ignore this function, if you don't like it.
real_t square (real_t value_a)
{
  return value_a * value_a;
}


// @task: Write a function to compute the error as demanded in 3.2b.
//        The function also depends on beta (the sheet is wrong).
//        This code is just a suggestion. You may remove it.

real_t compute_error (const matrix& w_a, real_t alpha_a, real_t beta_a)
{
  // @note: This lets you directly use  cos  and  cosh.
  using std::cos;
  using std::cosh;

    real_t sum1 = cosh(beta_a * w_a(0,0)) + cosh(beta_a * w_a(1,0)) + cosh(beta_a * w_a(2,0));
    real_t sum2 = -1.0 * alpha_a * cos(w_a(0,0)) * cos(w_a(1,0)) * cos(w_a(2,0)) + square(alpha_a);
    real_t error = sum1 + sum2 ;

    return error;
}


// @task: Write a function to compute the gradient as demanded in 3.2b.
//        This code is just a suggestion. You may remove it.
//        If you want to check whether your derivatives were correct, here is
//        mine for w1, where a = alpha and b = beta:
//  dE/dw1 = b * sinh(b * w1) + a * sin(w1) * cos(w2) * cos(w3)

matrix compute_gradient (const matrix& w_a, real_t alpha_a, real_t beta_a)
{
  // @note: Use  cos,  sin,  sinh  directly without "std::".
  using std::cos;
  using std::sin;
  using std::sinh;

  // Create matrix and fill with gradient.
    matrix gradient(3,1);
    gradient(0,0) = beta_a * sinh(beta_a * w_a(0,0)) + alpha_a * sin(w_a(0,0)) * cos(w_a(1,0)) * cos(w_a(2,0));
    gradient(1,0) = beta_a * sinh(beta_a * w_a(1,0)) + alpha_a * cos(w_a(0,0)) * sin(w_a(1,0)) * cos(w_a(2,0));
    gradient(2,0) = beta_a * sinh(beta_a * w_a(2,0)) + alpha_a * cos(w_a(0,0)) * cos(w_a(1,0)) * sin(w_a(2,0));

    return gradient;
}


// @task: Implement gradient descent as demanded in 3.2c.
//        Consider the stopping criterion well. You might need more than one
//        kind of criterion. Remember that the procedure may diverge! It
//        doesn't need to be elegant.
//        In case you have problems with values turning to NaN, you can
//        additionally use  std::isnan(value)  in your criterion.
//        This code is just a suggestion. You may remove it.

matrix descent (matrix w_a, real_t alpha_a, real_t beta_a, real_t eta_a)
{
    // @note: You may need  abs  to compute the absolute of a value.
    using std::abs;

    // It's probably good to have some introductory output here:
    std::cout << "Starting gradient descent at w_init = ("
              << w_a(0, 0) << "  " << w_a(1, 0) << "  " << w_a(2, 0) << ")^T with alpha = "
              << alpha_a << ", beta = " << beta_a << " and eta = " << eta_a << "\n";

    // In case you need a small value, this choice should be okay.
    const real_t epsilon_l = std::numeric_limits<real_t>::epsilon() * 128.0;

    // Depending on your approach, you might want to do one step of descent
    // outside the loop.

    real_t error_l = 0, error_old;
    matrix gradient_l,gradient_descent;

    matrix w_l = w_a;

    size_t maxIterations = 100000;
    size_t counter_l;
    for (counter_l = 0; counter_l < maxIterations; counter_l++) // stopping criterion for divergence (when you reach maxIterations)
    {
        gradient_l = compute_gradient(w_l, alpha_a, beta_a);
        gradient_descent = gradient_l * (-1.0 * eta_a);
        matrix w_old = w_l;
        w_l = w_old + (gradient_descent);

        error_l = compute_error(w_l, alpha_a, beta_a);
        error_old = compute_error(w_old, alpha_a, beta_a);


        // Now consider your stopping criterion and continue the descent until you
        // think it's enough.

        if (abs(error_l - error_old) < epsilon_l) // stopping criterion for convergence
        {
            std::cout << "stopped by convergence" << std::endl;

            break;
        }
    }
    // Now compute your gradient at the current position w_a.
    // This gradient and the learning rate eta are not themselfs the new
    // weights - they give you the values to change w_a by.

    // You'll probably want to print your progress from time to time and also
    // once your done with the loop.
    // Maybe someting like:
    //    std::cout << "After iteration " << counter_l << ":"
    //     << "  error = " << error_l
    //      << "  at w = (" << w_l(0,0) << "  " << w_l(1,0) << "  " << w_l(2,0) << ")^T"
    //      << "  gradient = (" << gradient_l(0,0)
    //      << "  "             << gradient_l(1,0)
    //     << "  "             << gradient_l(2,0) << ")^T\n";

    // Now return your final weight, though you don't really need that, if you
    // printed all relevant information from this function. Replace return type
    // matrix  with  void, if you don't want to return anything.

    if(counter_l == maxIterations)
    {
        std::cout << "stopped by divergence" << std::endl;
    }

    std::cout << "After iteration " << counter_l << ":"
     << "  error = " << error_l
      << "  at w = (" << w_l(0,0) << "  " << w_l(1,0) << "  " << w_l(2,0) << ")^T"
      << "  gradient = (" << gradient_l(0,0)
      << "  "             << gradient_l(1,0)
     << "  "             << gradient_l(2,0) << ")^T\n\n";


    return w_l;
}



matrix calc_hesse(const matrix& w, real_t alpha_a, real_t beta_a)
{
    using std::cos;
    using std::cosh;
    using std::sin;

    //hesse matrix
    matrix hesse(3,3);
    hesse(0,0) = square(beta_a) * cosh(beta_a * w(0,0)) + alpha_a * cos(w(0,0)) * cos(w(1,0)) * cos(w(2,0));
    hesse(1,1) = square(beta_a) * cosh(beta_a * w(1,0)) + alpha_a * cos(w(0,0)) * cos(w(1,0)) * cos(w(2,0));
    hesse(2,2) = square(beta_a) * cosh(beta_a * w(2,0)) + alpha_a * cos(w(0,0)) * cos(w(1,0)) * cos(w(2,0));

    hesse(0,1) = alpha_a * sin(w(0,0)) * (-1.0 * sin(w(1,0))) * cos(w(2,0));
    hesse(0,2) = alpha_a * sin(w(0,0)) * cos(w(1,0)) * (-1.0 * sin(w(2,0)));
    hesse(1,2) = alpha_a * cos(w(0,0)) * sin(w(1,0)) * (-1.0 * sin(w(2,0)));

    hesse(1,0) = hesse(0,1);
    hesse(2,0) = hesse(0,2);
    hesse(2,1) = hesse(1,2);

    return hesse;
}

int main(int argc, char** argv)
{
  // @task: Follow instructions from 3.2d:
  // @note: Remove these curly braces, if they bother you. Their only purpose
  //        is to prevent mixing up variables from tasks d and e.
  {
    real_t alpha_l = 5.0; 
    real_t beta_l = 0.1;

    // @task: Try a few (5) different learning rates. Make sure to use
    //        both the minimum 0.01 and the maximum 0.5.

    std::cout << "\nFirst Part:\n-----------------------------------------------------------" << std::endl;
    for(real_t eta_l = 0.01; eta_l < 0.51; eta_l += 0.07)
    {
        std::cout << "eta_l: " << eta_l << "\n-----------------------------------------------------------" << std::endl;
        // @task: For each learning rate try some (10) initial random weight vectors.
        //        You can create such random vector by calling  fill_with_uniform_samples(-5, 5)
        //        on a matrix of the right size. You might prefer the interval [-5, 5]
        //        over [-10, 10] from the sheet, because of a higher chance to
        //        maybe find a global optimum.
        //        The lowest value I found was 23 (cut after 4 decimal digits).
        for(size_t i = 0 ; i < 10; i++)
        {
            matrix w(3,1);
            w.fill_with_uniform_samples(-5,5);
            descent(w,alpha_l,beta_l,eta_l); //dont need return value here
        }
    }
    // @task: For which of the learning rates eta you used did the procedure diverge?
    //        It can also help to look at the gradient to see divergence.
    //        What's eta's impact on runtime (number of iterations)?
  }

  // @task: Follow instructions from 3.2e:
  {
    real_t alpha_l = -5.0; 
    real_t beta_l = 0.1;
    real_t eta_l = 0.01;

    // @task: Try the specified values.
    std::cout << "\nSecond Part:\n-----------------------------------------------------------" << std::endl;
    matrix w(3,1); //initialized with zeros
    matrix w_res = descent(w,alpha_l,beta_l,eta_l);
    std::cout << "Hesse matrix:\n" << calc_hesse(w_res,alpha_l,beta_l) << std::endl;

    w(0,0) = 1.5;
    w(1,0) = 1.5;
    //w(2,0) stays zero
    w_res = descent(w,alpha_l,beta_l,eta_l);
    std::cout << "Hesse matrix:\n" << calc_hesse(w_res,alpha_l,beta_l) << std::endl;

    // @task: To reason why no minimum is reached you may feel an urge to
    //        compute the eigenvalues of the Hesse matrix of second
    //        derivatives. You may, of course, follow that urge, but you don't
    //        have to. It's sufficient to make an educated guess. If you can't,
    //        it might already help to look at a plot of the error function
    //        with only one weight:  cosh(b*w) -a*cos(w)
    //        Such plots may be posted in Moodle.
  }

  return 0;
}
